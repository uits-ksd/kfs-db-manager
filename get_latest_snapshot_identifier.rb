#!/usr/bin/env ruby

require 'aws-sdk'
require 'logger'

STDOUT.sync = true
SLEEP_INTERVAL = 30
@logger = Logger.new(STDOUT)
@logger.level = Logger::INFO

usage = 'usage: ruby get_latest_snapshot_identifier.rb source_database_name
  database_instance_identifier - Name of the database used to create the desired snapshot.'
if ARGV.length != 1
  print usage
  exit
end

@source_db = ARGV[0]

DEFAULT_REGION = ENV['AWS_DEFAULT_REGION'] || 'us-west-2'
@rds = ::Aws::RDS::Client.new(region: DEFAULT_REGION)
response = @rds.describe_db_snapshots(db_instance_identifier: @source_db)
@snapshots = response[:db_snapshots]
@snapshots.sort_by! { |snap| snap[:snapshot_create_time] }
@snapshot_id = @snapshots[-1][:db_snapshot_identifier]
print @snapshot_id
