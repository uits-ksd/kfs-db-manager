#!/usr/bin/env ruby

require 'aws-sdk'
require 'logger'

STDOUT.sync = true
SLEEP_INTERVAL = 30
@logger = Logger.new(STDOUT)
@logger.level = Logger::INFO

@source_db = ARGV[0] || 'kfs6upg'

DEFAULT_REGION = ENV['AWS_DEFAULT_REGION'] || 'us-west-2'
@rds = ::Aws::RDS::Client.new(region: DEFAULT_REGION)
response = @rds.describe_db_snapshots(db_instance_identifier: @source_db)
@snapshots = response[:db_snapshots]
@snapshots.sort_by! { |snap| snap[:snapshot_create_time] }
@snapshots.reverse.each do |snap|
  if snap[:db_snapshot_identifier].include? 'upgraded'
    @snapshot_id = snap[:db_snapshot_identifier]
    break
  end
end
print @snapshot_id
