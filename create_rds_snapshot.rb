require 'aws-sdk'
require 'logger'
require 'date'

usage = 'usage: ruby create_rds_snapshot.rb snapshot_name_base database_instance_identifier user_netid jira_ticket
  snapshot_name_base - String to suffix with a timestamp to generate the name of the snapshot to take
  database_instance_identifier - String of the AWS Database Instance Identifier of the database to take a snapshot of
  user_netid - NetID of user creating snapshot
  jira_ticket - Jira ticket related to this snapshot
  deletion_deadline - Earliest date that the RDS snapshot will be deleted'

if ARGV.length != 5
  print usage
  exit
end


STDOUT.sync = true
SLEEP_INTERVAL = 30
@logger = Logger.new(STDOUT)
@logger.level = Logger::INFO

DEFAULT_REGION = ENV['AWS_DEFAULT_REGION'] || 'us-west-2'
@rds = ::Aws::RDS::Client.new(region: DEFAULT_REGION)

@snapshot_id_base = ARGV[0]
@db_instance_identifier = ARGV[1]
@contact_netid = ARGV[2]
@jira_ticket = ARGV[3]
@deletion_deadline = ARGV[4]

@snapshot_tags = [
    {
      key: "createdby",
      value: @contact_netid,
    },
    {
      key: "contactnetid",
      value: @contact_netid,
    },
    {
      key: "ticketnumber",
      value: @jira_ticket,
    },
    {
      key: "deleteafter",
      value: @deletion_deadline,
    },
  ]

@snapshot_id = DateTime.now.strftime(@snapshot_id_base + '-%Y-%m-%d-%H%M')
snapshot_resp = @rds.create_db_snapshot(db_snapshot_identifier: @snapshot_id,
                        db_instance_identifier: @db_instance_identifier)
resource_arn = snapshot_resp.db_snapshot.db_snapshot_arn
@logger.info "Snapshot ARN: \"#{resource_arn}\""

# wait for snapshot to finish
instance = @rds.describe_db_snapshots(db_snapshot_identifier: @snapshot_id)
status = instance[:db_snapshots][0][:status]
@logger.info "Waiting for snapshot of \"#{@db_instance_identifier}\" named \"#{@snapshot_id}\". Current status is: \"#{status}\""
while status != 'available' do
  sleep(SLEEP_INTERVAL)
  instance = @rds.describe_db_snapshots(db_snapshot_identifier: @snapshot_id)
  status = instance[:db_snapshots][0][:status]
  @logger.info "Waiting for snapshot of \"#{@db_instance_identifier}\" named \"#{@snapshot_id}\". Current status is: \"#{status}\""
end

# update tags on snapshot
@rds.add_tags_to_resource({
  resource_name: resource_arn,
  tags: @snapshot_tags
})
