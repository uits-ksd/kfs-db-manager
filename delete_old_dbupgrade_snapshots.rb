#!/usr/bin/env ruby

require 'aws-sdk'
require 'logger'
require 'optparse'

@options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: delete_old_dbupgrade_snapshots.rb [options]"

  opts.on("-x", "--execute", "Actually execute calculated delete operations. If not specified, a dry run will be executed.") do |x|
    @options[:execute] = x
  end

  opts.on("-kNUMBER", "--keep=number", "Latest number of snapshots to keep and not delete.") do |k|
    @options[:keep] = k
  end

  opts.on("-h", "--help", "Prints this help") do
    puts opts
    exit
  end

  opts.on("--snapshot-prefix=kfs#upg[d?]-upgrade", "REQUIRED - Snapshot prefix to search for and clean up") do |p| 
    @options[:prefix] = p
  end
end.parse!

STDOUT.sync = true
SLEEP_INTERVAL = 30
DEFAULT_REGION = ENV['AWS_DEFAULT_REGION'] || 'us-west-2'
@logger = Logger.new(STDOUT)
@logger.level = Logger::INFO

@rds = ::Aws::RDS::Client.new(region: DEFAULT_REGION)
@s3 = Aws::S3::Client.new(region: DEFAULT_REGION)
@tst_snapshot_to_exclude = @s3.get_object(bucket: 'kfs-build-artifacts', key: 'KFS7TST_source_snapshot_id.txt').body.read.strip
print "Excluding snapshot used to create TST during last database refresh: " + @tst_snapshot_to_exclude + "\n"
def get_snapshots_with_prefix(prefix)
  response = @rds.describe_db_snapshots
  snapshots_raw = response[:db_snapshots]
  #convert snapshot_create_time to integer, as if a snapshot is being created
  # it will have a create time of 'nil' which breaks the sort
  snapshots_raw.sort_by! { |snap| snap[:snapshot_create_time].to_i }
  snapshots = Array.new
  snapshots_raw.reverse.each do |snap|
    if snap[:db_snapshot_identifier].start_with? prefix
      snapshots.push(snap[:db_snapshot_identifier])
    end
  end
  return snapshots
end

def delete_snapshots_with_prefix_and_keep(prefix, keep)
  snapshots = get_snapshots_with_prefix(prefix)
  puts "Snapshots:"
  puts(snapshots)
  #keep the latest 'keep' snapshots, so remove the first 'keep' items
  # (and keep is 1-based indexing, not 0, so start at 1)
  snapshots.shift(keep)
  delete_snapshots(snapshots)
  return snapshots
end

def delete_snapshots(snapshots)
  snapshots.each do |snap|
    if(snap == @tst_snapshot_to_exclude)
      print "SKIPPING snapshot used for last data refresh to TST: " + @tst_snapshot_to_exclude + "\n"
      next
    end
    if(@options[:execute]) then
      @rds.delete_db_snapshot(db_snapshot_identifier: snap)
      print "Deleted: " + snap + "\n"
    else
      print "DRY RUN - (would have) Deleted: " + snap + "\n"
    end
  end  
end

# By default, keep most recent 2 snapshots of each type
@keep_arg = if @options[:keep] != nil then @options[:keep].to_i else 2 end
@snapshot_prefix = nil
if (@options[:prefix] != nil) then
  @snapshot_prefix=@options[:prefix]
end
if(@snapshot_prefix == nil) then
  raise('--snapshot-prefix must be specified')
end

# do the actual execution
delete_snapshots_with_prefix_and_keep(@snapshot_prefix, @keep_arg)
