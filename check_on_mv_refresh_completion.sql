DECLARE
  CURSOR check_status IS
    select count(1)
    from mv_refresh_queue
    where status = 'In Progress';

 v_cnt NUMBER;
BEGIN

 LOOP

   OPEN check_status;
      FETCH check_status INTO v_cnt;
    CLOSE check_status;

   IF v_cnt = 0 THEN
      EXIT;
    END IF;

   --Sleep for 5 seconds
    DBMS_LOCK.SLEEP(5);

 END LOOP;

END;
/