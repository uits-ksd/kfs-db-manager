require 'aws-sdk'
require 'logger'

STDOUT.sync = true
SLEEP_INTERVAL = 30
@logger = Logger.new(STDOUT)
@logger.level = Logger::INFO

DEFAULT_REGION = ENV['AWS_DEFAULT_REGION'] || 'us-west-2'
@rds = ::Aws::RDS::Client.new(region: DEFAULT_REGION)

usage = 'usage: ruby kfs_dbupgrade_delete.rb database_instance_identifier
  database_instance_identifier - String of the AWS Database Instance Identifier of the database to delete'

if ARGV.length != 1
  print usage
  exit
end

@db_instance_identifier = ARGV[0]
@delete_opts = {
    :db_instance_identifier => @db_instance_identifier,
    :skip_final_snapshot => true
}

@logger.info 'Deleting database.'
@rds.delete_db_instance(@delete_opts)

instance = @rds.describe_db_instances(db_instance_identifier: @db_instance_identifier)
status = instance[:db_instances][0][:db_instance_status]
@logger.info "Deleting RDS instance \"#{@db_instance_identifier}\". Current status is \"#{status}\"..."
begin
  while true do
    sleep(SLEEP_INTERVAL)
    instance = @rds.describe_db_instances(db_instance_identifier: @db_instance_identifier)
    status = instance[:db_instances][0][:db_instance_status]
    @logger.info "Deleting RDS instance \"#{@db_instance_identifier}\". Current status is \"#{status}\"..."
  end
rescue ::Aws::RDS::Errors::DBInstanceNotFound
  @logger.info "RDS instance \"#{@db_instance_identifier}\" deleted."
end