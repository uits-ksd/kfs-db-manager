require 'aws-sdk'
require 'logger'
require 'date'

usage = 'usage: ruby kfs_dbupgrade_snapshot.rb snapshot_name_base database_instance_identifier
  snapshot_name_base - String to suffix with a timestamp to generate the name of the snapshot to take
  database_instance_identifier - String of the AWS Database Instance Identifier of the database to take a snapshot of'

if ARGV.length != 2
  print usage
  exit
end


STDOUT.sync = true
SLEEP_INTERVAL = 30
@logger = Logger.new(STDOUT)
@logger.level = Logger::INFO

DEFAULT_REGION = ENV['AWS_DEFAULT_REGION'] || 'us-west-2'
@rds = ::Aws::RDS::Client.new(region: DEFAULT_REGION)

@snapshot_id_base = ARGV[0]
@db_instance_identifier = ARGV[1]

@snapshot_id = DateTime.now.strftime(@snapshot_id_base + '-%Y-%m-%d-%H%M')
@rds.create_db_snapshot(db_snapshot_identifier: @snapshot_id,
                        db_instance_identifier: @db_instance_identifier)

# wait for snapshot to finish
instance = @rds.describe_db_snapshots(db_snapshot_identifier: @snapshot_id)
status = instance[:db_snapshots][0][:status]
@logger.info "Waiting for snapshot of \"#{@db_instance_identifier}\" named \"#{@snapshot_id}\". Current status is: \"#{status}\""
while status != 'available' do
  sleep(SLEEP_INTERVAL)
  instance = @rds.describe_db_snapshots(db_snapshot_identifier: @snapshot_id)
  status = instance[:db_snapshots][0][:status]
  @logger.info "Waiting for snapshot of \"#{@db_instance_identifier}\" named \"#{@snapshot_id}\". Current status is: \"#{status}\""
end
